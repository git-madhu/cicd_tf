#root

module "vpc" {
    source = "./vpc"
  
}

module "ec2" {
    source = "./web"
    mysubnet = module.vpc.subnet_id
    sg = module.vpc.security_group_id
}
