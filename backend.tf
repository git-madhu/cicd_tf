# create s3 - to store statefile

resource "aws_s3_bucket" "bucket" {
  bucket = "tf-state1-bucket1"

  }

resource "aws_s3_bucket_versioning" "bucket_versioning" {
  bucket = aws_s3_bucket.bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}  


resource "aws_s3_bucket_server_side_encryption_configuration" "bucket_encryption" {
  bucket = aws_s3_bucket.bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}


# create dynamo DB table - to lock the statefile

resource "aws_dynamodb_table" "state_lock" {
  name           = "state_lock"
  billing_mode   = "PAY_PER_REQUEST"
#  read_capacity  = 20
#  write_capacity = 20
  hash_key       = "LockID"
#  range_key      = "GameTitle"

  attribute {
    name = "LockID"
    type = "S"
  }
}

# For locking & migrating the staefile to s3

#terraform {
#  backend "s3" {
#    bucket = "tf-state1-bucket1"
#    key    = "state"
#    region = "us-east-1"
#    dynamodb_table = "state_lock"
#    encrypt = true
#  }
#}
